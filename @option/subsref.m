function b = subsref(a,index)
% SUBSREF for option objects
%
% Syntax:
%  b = SUBSREF(a,index)
%
% Usage:
%   <fieldvalue> = option.<fieldname>
%
%  See also ITEMS.

%   Author(s): Akos Szoke
%   Copyright (c) 2006
%   $Revision: 1.0 $  $Date: 2007/01/05 15:41:44 $

switch index(1).type
case '.'
    try
        b = a.(index(1).subs); % using dynamic fields
    catch ME
        disp('Invalid field reference!');
        rethrow(ME);
    end
otherwise
   error('Subscripted reference error occured!')
end

% --- EOF ---

