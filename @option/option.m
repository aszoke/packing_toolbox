function option = option(varargin)
%OUTPUT Creation of option object.
%
% Class constructors:
%    opt = option() creates a option object
%                   - Verbose:  0: (default) diplays no input
%                               1: diplays input for iterations
%                   - MaxIter: []: no barrier
%                               n: maximum number of iteration
%                   - Strategy: heuristics strategy (if applicapable)
%
%   Author(s): Akos Szoke
%   Copyright (c) 2006
%   $Revision: 1.0 $  $Date: 2007/01/05 15:41:44 $

% -- constructor for no input
if (nargin == 0) 
  
    % Create the structure
    opt = struct(...
        'Verbose', 0,...
        'MaxIter', [],...
        'Strategy', '');

    option = class(opt, 'option'); 
    
% -- constructor for other input
else
    error('Unsupported constructor!');
end
    
% --- EOF ---
