function display(opt)
%DISPLAY Display option object
%
% Syntax
%    DISPLAY(opt)
%  
% See also OPTION.

%   Author(s): Akos Szoke
%   Copyright (c) 2006
%   $Revision: 1.0 $  $Date: 2007/01/05 15:41:44 $

disp(' --- Option object --- ');
disp([' Verbose:                 ', num2str(opt.Verbose)]);
disp([' MaxIter:                 ', num2str(opt.MaxIter)]);
disp([' Strategy:                ', opt.Strategy]);

% --- EOF ---
