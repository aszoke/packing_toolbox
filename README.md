# Packing Toolbox

## About

Packing Toolbox contains knapsacking and binpacking algorithms. Knapsacking and binpacking problems are a class of optimization problems that involve attempting to pack objects together into containers. 

**Author**: Akos Szoke <aszoke@mit.bme.hu>

**Web site**: [http://www.kese.hu](http://www.kese.hu)

**Toolbox Source**: [https://bitbucket.org/aszoke/](https://bitbucket.org/aszoke/)
 
### Features

**Binpacking algorithms**

* **bbpgreedy** - greedy bin-packing algorithms with packing strategies (approximate algorithm: provides local optimum)

**Knapsack algorithms**

* **bkpbb1** - 0-1 knapsack branch and bound algorithm (based on Horowitz-Sahni /1974/)
* **bkpbruteforce** - 0-1 knapsack brute force algorithm 
* **bkpci1** - 0-1 knapsack algorithm computing the critical item
* **bkpdpI1** - BKPDP1 0-1 knapsack dynamic programming algorithm for only integer(!) weights
* **bkpdpI2** - BKPDP2 0-1 knapsack dynamic programming with memoization (memory function)
* **bkpgreedy** - 0-1 knapsack greedy algorithm (approximate algorithm: provides local optimum)
* **bkpreduct1** - 0-1 knapsack algorithm <fname> with reduction
* **ckp1** - continuous knapsack algorithm (linear programming approach)
* **ckp2** - continuous knapsack algorithm (linear programming relaxation approach)

**Scheduling (special packing) algorithms**

* **binsched** - greedy bin-packing algorithms with packing strategies (approximate algorithm: provides local optimum)
* **mksched** - multi 0-1 knapsack branch and bound algorithm upgraded with priority and SOFT precedence (based on Horowitz-Sahni /1974/)
* **mksched2** - MKSCHED multi 0-1 knapsack branch and bound algorithm upgraded with priority and HARD precedence (based on Horowitz-Sahni /1974/)
* **mksched4mex** - MEX version of mksched2 see: [Introducing MEX-Files](http://www.mathworks.com/help/matlab/matlab_external/introducing-mex-files.html)

### Theoretical Notes

**LOWER BOUND**: lower bound on the approximation algorithm solution (running time  is bounded by polinomial in the input size)

For a maximization problem, the solution value determined by an approximate algorithm limits the optimal solution value from below. It is always convenient to have methods for limiting this value from above, too: UPPER BOUND.

**UPPER BOUND**:

* in enumerative algorithm: to exclude computations which cannot lead to the optimal solution
* in approximation algorithm: to "a-posteriori" evaluate the performance obtained Upper bounds are usually computed by solving relaxations such as continuous, Lagrangian and surrogate.

**RELATIVE ERROR** of an approximation algorithm: relative error of the approximate solution is no greater than (U(I)-A(I))/U(I) -- where I is the input; U is the upperbound value, A is the approximation algorithm solution value

**WORST CASE PERFORMANCE RATIO** of an approximation algorithm: the smallest real number such that: U(I)/OPT(I)<=r(U) -- where OPT is the optimal solution value

### Dependencies

* [GNU Linear Programming Kit](http://www.gnu.org/software/glpk/) - only
for [glpkwrap.m](./packing_toolbox/src/master/utils/glpkwrap.m). It requires the [GLPKMEX](http://glpkmex.sourceforge.net/) Matlab
toolbox.

## Usage

using the functions in the feature list

### Example

 * [test_packing_toolbox.m](./packing_toolbox/src/master/test_packing_toolbox.m)

The generated log of the example can be found in
[test_packing_toolbox.html](https://bitbucket.org/aszoke/packing_toolbox/raw/master/html/test_packing_toolbox.html).

**Note**: *Due to the Bitbucket HTML preview restriction (see [HTML rendering for generated doc](https://bitbucket.org/site/master/issue/6353/html-rendering-for-generated-doc)), in order to view the Test run samples in HTML, use should use the [GitHub & BitBucket HTML Preview](http://htmlpreview.github.io/) service. (Copy the link below and paste into the text box that can be found in the service.)*

### Test data

The test data can be generated with [generate_data.m](./packing_toolbox/src/master/generate_data.m)

* [data_binpack.mat](./packing_toolbox/src/master/data_binpack.mat)
* [data_knapsack.mat](./packing_toolbox/src/master/data_knapsack.mat)

## License

(The MIT License)

Copyright (c) 2011 Akos Szoke

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.