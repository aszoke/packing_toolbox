
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    DATA FILE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


disp('---=== Data save procedure started ===---.');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% KNAPSACK DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
i = 0;
% ------ see Example 2.1 from Martello:knp90 - page 21
i = i+1;
dataset{i}.w    = [2 20 20 30 40 30 60 10];             % weight
dataset{i}.p    = [15 100 90 60 40 15 10 1];            % profit
dataset{i}.name = 'Martello-Example2.1';
capacity{i}     = 102;                                  % capacity

% ------ see Example 2.2 from Martello:knp90 - page 32
i = i+1;
dataset{i}.w    = [31 10 20 19 4 3 6];                  % weight
dataset{i}.p    = [70 20 39 37 7 5 10];                 % profit
dataset{i}.name = 'Martello-Example2.2';
capacity{i}     = 50;                                   % capacity

% ------ see Example 8.1 from Martello:knp90 - page 230
i = i+1;
dataset{i}.w    = [70 60 50 33 33 33 11 7 3];               % weight
dataset{i}.p    = dataset{i}.w;                             % profit
dataset{i}.name = 'Martello-Example8.1';
capacity{i}     = 100;       % capacity

% ------ see Example 8.2 from Martello:knp90 - page 236
i = i+1;
dataset{i}.w    = [99 94 79 64 50 46 43 37 32 19 18 7 6 3]; % weight
dataset{i}.p    = dataset{i}.w;                             % profit
dataset{i}.name = 'Martello-Example8.2';
capacity{i}     = 100;                                      % capacity

% ------ see Example 8.3 from Martello:knp90 - page 238
i = i+1;
dataset{i}.w    = [49 41 34 33 29 26 26 22 20 19];          % weight
dataset{i}.p    = dataset{i}.w;                             % profit
dataset{i}.name = 'Martello-Example8.3';
capacity{i}     = 100;                                      % capacity

clear i;
save data_knapsack;

disp('Saved: KNAPSACK DATA.');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BINPACK DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
i = 0;
% ------ see Example 2.1 from Martello:knp90 - page 21
i = i+1;
dataset{i}.w    = [2 20 20 30 40 30 60 10];             % weight
dataset{i}.p    = [15 100 90 60 40 15 10 1];            % profit
dataset{i}.name = 'Martello-Example2.1';
capacity{i}     = 102;                                  % capacity

% ------ see Example 2.2 from Martello:knp90 - page 32
i = i+1;
dataset{i}.w    = [31 10 20 19 4 3 6];                  % weight
dataset{i}.p    = [70 20 39 37 7 5 10];                 % profit
dataset{i}.name = 'Martello-Example2.2';
capacity{i}     = 50;                                   % capacity

% ------ see Example 8.1 from Martello:knp90 - page 230
i = i+1;
dataset{i}.w    = [70 60 50 33 33 33 11 7 3];               % weight
dataset{i}.p    = dataset{i}.w;                             % profit
dataset{i}.name = 'Martello-Example8.1';
capacity{i}     = 100;       % capacity

% ------ see Example 8.2 from Martello:knp90 - page 236
i = i+1;
dataset{i}.w    = [99 94 79 64 50 46 43 37 32 19 18 7 6 3]; % weight
dataset{i}.p    = dataset{i}.w;                             % profit
dataset{i}.name = 'Martello-Example8.2';
capacity{i}     = 100;                                      % capacity

% ------ see Example 8.3 from Martello:knp90 - page 238
i = i+1;
dataset{i}.w    = [49 41 34 33 29 26 26 22 20 19];          % weight
dataset{i}.p    = dataset{i}.w;                             % profit
dataset{i}.name = 'Martello-Example8.3';
capacity{i}     = 100;                                      % capacity

% ------ MyExampleBin01
i = i+1;
dataset{i}.w    = [1 4 2 1 2 3 5];                          % elements that must be put into a bin
dataset{i}.p    = dataset{i}.w;
dataset{i}.name = 'MyExampleBin01';
capacity{i}     = 5; %[5 7 8 5 6 5 6];                      % capacity vector of bins

% ------ MyExampleBin02
i = i+1;
dataset{i}.w    = [1 2 1 3 1.5 2.1 3];                      % elements that must be put into a bin
dataset{i}.p    = dataset{i}.w;
dataset{i}.name = 'MyExampleBin02';
capacity{i}     = 5; %[5 7 8 5 6 5 6];                      % capacity vector of bins

% ------ MyExampleBin03
i = i+1;
dataset{i}.w    = [11 10 20 19 4 3 6];                      % elements that must be put into a bin
dataset{i}.p    = [1 1 1 1 1 1 1];
dataset{i}.name = 'MyExampleBin03';
capacity{i}     = 35; %[35 35 35 35 35 35 35];              % capacity vector of bins

% ------ MyExampleBin04
i = i+1;
dataset{i}.w    = [1 4 2 1 2 3 5];                          % weight
dataset{i}.p    = [2 1 3 3 2 1 1];                          % profit
dataset{i}.name = 'MyExampleBin04';
capacity{i}     = 10; %[10 7 8 9 12 5 10];                  % capacity

clear i;
save data_binpack;

disp('Saved: BINPACK DATA.');

disp('---=== Data save procedure ended ===---.');
% --- EOF ---
