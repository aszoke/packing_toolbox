function [x,z,ndx,exitflag,out] = knapsack_frame(fname,itm,cap,opt)
%KNAPSACK_FRAME algorithm frame for different knapsack algorithms
%
% Syntax:
%     [x,z,ndx,exitflag,output] = knapsack_frame(fname,items,cap,opt)
%   Input params:
%      fname        - knapsacking algorithm's name
%      items        - packagable elements (with profit and weights)
%      cap          - capacity of the knapsack
%      opt          - (optional) options for the algorithm execution
%                   - verbose: 0: (default) diplays no input
%                              1: diplays input for iterations
%   Return values:
%      x            - unknowns /0,1/ (selected elements into the knapsack)
%      z            - objective (profit)
%      ndx          - index of selected items
%      exitflag     - for identifying the reason the algorithm terminated
%                       0: terminated normally
%                       1: terminated
%      out          - output object
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- function body --

prf = itm.Profit;
wgt = itm.Weight;

% computing the knapsack problem
if strcmp(fname,'bkpreduct1')
    fh = str2func('bkpbb1');        % in this case this is the default
else
    fh = str2func(fname);           % function handle (determines algorithm)
end;

tic;                % --> START computation stopwatch

[x,z,ndx,exitflag,itc] = fh(prf,wgt,cap,opt); 

ctime = toc;        % --> STOP computation stopwatch

% --- set output properties ---

out = output();

out.Iteration           = itc;
out.DatasetName         = itm.DatasetName;

if exitflag == 0        % % terminated normally
    out.PackingAlgorithm    = fname;
    out.IsPacked            = 1;    % TRUE
    out.Solution            = x;
    out.SolutionValue       = z;
    out.PackedWeight        = wgt * x';
    out.Bins                = 1;    
    out.Gap                 = gap(single(wgt(ndx)*x(ndx)'),cap);  % casting to single for continuous variables' rounding!
    out.SolvingTime         = ctime;
    out.Score               = score(out.Gap,ctime);
    out.Notes               = '';
    out.Version             = 1.0;
else
    out.PackingAlgorithm    = ['*', fname];
end

end
% --- EOF ---