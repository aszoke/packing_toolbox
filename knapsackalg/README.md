# Knapsack Algorithms within Packing Toolbox

### Features

* **bkpbb1** - 0-1 knapsack branch and bound algorithm (based on Horowitz-Sahni /1974/)
* **bkpbruteforce** - 0-1 knapsack brute force algorithm 
* **bkpci1** - 0-1 knapsack algorithm computing the critical item
* **bkpdpI1** - BKPDP1 0-1 knapsack dynamic programming algorithm for only integer(!) weights
* **bkpdpI2** - BKPDP2 0-1 knapsack dynamic programming with memoization (memory function)
* **bkpgreedy** - 0-1 knapsack greedy algorithm (approximate algorithm: provides local optimum)
* **bkpreduct1** - 0-1 knapsack algorithm <fname> with reduction
* **ckp1** - continuous knapsack algorithm (linear programming approach)
* **ckp2** - continuous knapsack algorithm (linear programming relaxation approach)

* **knapsack_frame** - algorithm frame for different knapsack algorithms