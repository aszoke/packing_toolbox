function [x,z,ndx,exitflag,itc] = bkpbruteforce(prf,wgt,c,opt)
%BKPBRUTEFORCE 0-1 knapsack brute force algorithm 
%
% Syntax:
%     [x,z,ndx,exitflag,itc] = bkpbruteforce(prf,wgt,c,opt) 
%   Input params:
%      prf          - profit of elements
%      wgt          - weight of elements
%      c            - capacity of the knapsack
%      opt          - (optional) options for the algorithm execution
%                   - Verbose: 0: (default) diplays no input
%                              1: diplays input for iterations
%                   - MaxIter: []: no barrier
%                              n : maximum number of iteration
%   Return values:
%      x            - unknowns /0,1/ (selected elements into the knapsack)
%      z            - objective (profit)
%      ndx          - index of selected items
%      exitflag     - for identifying the reason the algorithm terminated
%                       0: terminated normally
%                       1: terminated because it is reached MaxIter
%      itc          - iteration count
%
% Complexity: Ordo(N*2^N) /plus O(nlogn) for the initial sorting/
% Space     : -
%
% Reference:
% -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

chkknapsackinput(prf,wgt,c);

% -- function body --
itc = 0;    % iteration counter

% ordering jobs according to prf/wgt
[b,ix] = sort(prf./wgt,'descend');
p = prf(ix);
w = wgt(ix);

% -- initialization

n = length(p);
zn = 0;         % currently the value of the best solution
wn = 0;         % currently the weight of the best weight
xn = [];        % currently the best solution
for i=0:2^n-1
    
    % ******* Administrative block - Start
    itc = itc + 1;  % increment iteration counter
    if opt.Verbose == 1      % dispay iteration info
        fprintf('%d.',itc);
    end;
    if opt.MaxIter == itc    % reached the maximum iteration
        exitflag = 1;
        x = [];
        z = -1;
        ndx = [];
        return;
    end;
    % ******* Administrative block - End
    
    % convert value to a solution vector
    bin = dec2bin(i);               % convert value to binary value    
    xcurr = zeros(1,length(bin));   % preallocation
    for j=length(bin):-1:1
        xcurr(j) = str2double(bin(j));       % build current solution vector
    end;
    xcurr = [zeros(1,n-length(bin)),xcurr];  %#ok<AGROW> % construct the whole solution vector
   
    % checking whether the computed solution is better than the optimal one
    % so far beside it has less than or equal weight as capacity FIXME!!!
    if (sum(w .* xcurr) <= c) ...       % not greater than the available capacity
    && ((sum(p .* xcurr) > zn) ...      % AND (greater than the actual solution value
        || (~(sum(p .* xcurr) < zn) ... %     OR (not smaller than the actual solution value 
            && (sum(w .* xcurr) > wn))) %         AND filled with grater capacity)
        xn = xcurr;
        wn = sum(w .* xcurr);
        zn = sum(p .* xcurr);
    end; 
end;
x = xn;

% recover the initial order
[b,ix2] = sort(ix);
x = x(ix2);

% determining the indeces
ndx = find(x);
z = zn;

exitflag = 0;   % terminated normally

end
% --- EOF ---
