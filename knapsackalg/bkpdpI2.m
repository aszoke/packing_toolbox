function [x,z,ndx,exitflag,itc] = bkpdpI2(prf,wgt,c,opt)
%BKPDP2 0-1 knapsack dynamic programming with memoization (memory function)
%algorithm for only integer(!) weights
%
% Syntax:
%     [x,z,ndx,exitflag,itc] = bkpdp2(prf,wgt,c,opt) 
%   Input params:
%      prf          - profit of elements
%      wgt          - weight of elements
%      c            - capacity of the knapsack
%      opt          - (optional) options for the algorithm execution
%                   - Verbose: 0: (default) diplays no input
%                              1: diplays input for iterations
%                   - MaxIter: []: no barrier
%                              n : maximum number of iteration
%   Return values:
%      x            - unknowns /0,1/ (selected elements into the knapsack)
%      z            - objective (profit)
%      ndx          - index of selected items
%      exitflag     - for identifying the reason the algorithm terminated
%                       0: terminated normally
%                       1: terminated because it is reached MaxIter
%      itc          - iteration count
%
% ========================================================================
%                       Comment on the operation
% ========================================================================
% This solution is very similar that is presented in BKPDP1 but recursion.
% Recursion presented here makes it possible to traject the solution space
% from top to bottom (contrary to the BKPDP1, which is bottom to top) which 
% makes it possible to provide a quicker solution (less steps as it checks 
% only the possible solution space), but requires more memory due to
% recursion.
% ========================================================================
%
% Complexity: Ordo(n) /initial sorting is not necessary/
% Space     : -
%
% Reference:
% 1) M. Hristakeva and D. Shrestha: Different Approaches to Solve the 0/1 Knapsack Problem
%    (The presented concept is useful, but there are many problems with the implementation in it.)
% 2)(Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 16, Dynamic programming

%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

chkknapsackinput(prf, wgt, c);

if ~isequal(wgt,fix(wgt))
    error('Weights must be integer values!');
end;

% -- function body --
itc = 0;    % iteration counter

n = length(prf);  % item count

% initialize global variables:
global gp;      % global profit
global gw;      % global weight
global gtbl;    % global table, which is used for the calculation
gp = prf; gw = wgt;
global gitc;    % global iteration counter
global gopt;    % global option object
global gteardown; % global teardown

gitc = itc;     % iteration counter
gopt = opt;
gteardown = 0;  % not

% initialize tabular data type that will contain the optimal solution:
% the first row contains zeros and the others contain '-1's
gtbl = horzcat(zeros(n+1,1),vertcat(zeros(1,c),(-1)*ones(n,c)));    %#ok<NASGU>

I = n+1;            % index for the table to easier understanding
J = c+1;            % detto
memoryfun(I,J);     % calling without result parameter as the result is in the 'gtbl'

if gteardown == 1
        exitflag = 1;
        x = [];
        z = -1;
        ndx = [];
        itc = gitc;
        return;
end        

% the following step is to construct the solution (determining which item is 
% included or excluded) from the table
% >>> Note: the following part is the same as in BKPDP1 <<<

xn = zeros(1,n);
for i=n:-1:1
    I = i + 1;                    % index for the table to easier understanding
    if gtbl(I,J) == gtbl(I-1,J)   % it means that ith item is not included in the solution
        xn(i) = 0;
    else
        xn(i) = 1;
        J = J-gw(i);
    end;
    % 
end;

% right-down corner element is the optimum
z = gtbl(n+1,c+1);
% constructed optimal solution
x = xn;
% determining the indeces
ndx = find(x);

itc = gitc;

exitflag = 0;   % terminated normally

end

% --------------------- SUBFUNCTION ----------------------------
function value = memoryfun(I, J)
% declaration of using global variables
global gp; global gw; global gtbl;
global gitc; global gopt;
global gteardown;

% >>> Note: the following part is very similar to as in BKPDP1 <<<
if gtbl(I,J) < 0                                                % if a possible solution is not checked
    
    % ******* Administrative block - Start
    if gopt.MaxIter <= gitc    % reached the maximum iteration
        gteardown = 1;
        value = [];
        return;
    else
        gitc = gitc + 1;  % increment iteration counter
    end
    if gopt.Verbose == 1      % dispay iteration info
        fprintf('%d.',gitc);
    end;
    % ******* Administrative block - End    
    
    if J-1 < gw(I-1)                                            % Cannot fit the ith item
        m = memoryfun(I-1,J);
        if gteardown == 1
            value = [];
            return;
        end        
        gtbl(I,J) = m;
    else             
        m1 = memoryfun(I-1,J);                                  % Do not use the ith item (*)
        m2 = memoryfun(I-1,J-gw(I-1)) + gp(I-1);                % Use the ith item (**)
        if gteardown == 1
            value = [];
            return;
        end
        gtbl(I,J) = max(m1,m2);    
    end;
end;
value = gtbl(I,J);
end

% --- EOF ---