function [x,z,ndx,exitflag,itc] = bkpgreedy(prf,wgt,c,opt)
%BKPGREEDY 0-1 knapsack greedy algorithm (approximate algorithm: provides local optimum)
%
% Syntax:
%     [x,z,ndx,exitflag,itc] = bkpgreedy(prf,wgt,c,opt) 
%   Input params:
%      prf          - profit of elements
%      wgt          - weight of elements
%      c            - capacity of the knapsack
%      opt          - (optional) options for the algorithm execution
%                   - Verbose: 0: (default) diplays no input
%                              1: diplays input for iterations
%                   - MaxIter: []: no barrier
%                              n : maximum number of iteration
%   Return values:
%      x            - unknowns /0,1/ (selected elements into the knapsack)
%      z            - objective (profit)
%      ndx          - index of selected items
%      exitflag     - for identifying the reason the algorithm terminated
%                       0: terminated normally
%                       1: terminated because it is reached MaxIter
%      itc          - iteration count
%
% Complexity: Ordo(N) /plus O(nlogn) for the initial sorting/
% Space     : Ordo(N)
% Worst-case performance ratio: Approx(I)/Optim(I)=1/2
%
% Reference:
% (Martello:knp90)
%   Martello and Toth - Knapsack problems: algorithms and computer implementations, 
%   John Wiley & Sons, Inc., 1990,
%   Chapter 2, 0-1 Knapsack problem
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

chkknapsackinput(prf,wgt,c);

% -- function body --
itc = 0;    % iteration counter

% ordering jobs according to prf/wgt
[b,ix] = sort(prf./wgt,'descend');
p = prf(ix);
w = wgt(ix);

n = length(p);
cn = c;         % remained capacity
x = [];         % assignment vector
z = 0;          % optimal value
jopt = 1;       % points out to the the element which has greatest profit
% iteration through elements
for j=1:n
    
    % ******* Administrative block - Start
    itc = itc + 1;  % increment iteration counter
    if opt.Verbose == 1      % dispay iteration info
        fprintf('%d.',itc);
    end;
    if opt.MaxIter == itc    % reached the maximum iteration
        exitflag = 1;
        x = [];
        z = -1;
        ndx = [];
        return;
    end;
    % ******* Administrative block - End    
    
    if w(j) > cn    % if element's weight is greater than the remained capacity
        x(j) = 0; %#ok<AGROW>
    else            
        x(j) = 1; %#ok<AGROW>
        cn = cn - w(j); % decrease the remained capacity
        z = z + p(j);   % compute the objective function's value
    end;
    if p(j) > p(jopt)
        jopt = j;
    end;
end;
% in degenerative case: an element has greater profit than the sum of
% previous one so in this case we choose this latter one
if p(jopt) > z
    z = p(jopt);
    x = zeros(n,1)';
    x(jopt) = 1;
end;

% recover the initial order
[b,ix2] = sort(ix);
x = x(ix2);

% determining the indeces
ndx = find(x);

exitflag = 0;   % terminated normally

end
% --- EOF ---
