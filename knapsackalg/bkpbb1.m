function [x,z,ndx,exitflag,itc] = bkpbb1(prf,wgt,c,opt)
%BKPBB1 0-1 knapsack branch and bound algorithm (based on Horowitz-Sahni /1974/)
% It assumes integrality of prf and wgt! (Otherwise it performs not so well.)
%
% Syntax:
%     [x,z,ndx,exitflag,itc] = bkpbb1(prf,wgt,c,opt) 
%   Input params:
%      prf          - profit of elements
%      wgt          - weight of elements
%      c            - capacity of the knapsack
%      opt          - (optional) options for the algorithm execution
%                   - Verbose: 0: (default) diplays no input
%                              1: diplays input for iterations
%                   - MaxIter: []: no barrier
%                              n : maximum number of iteration
%   Return values:
%      x            - unknowns /0,1/ (selected elements into the knapsack)
%      z            - objective (profit)
%      ndx          - index of selected items
%      exitflag     - for identifying the reason the algorithm terminated
%                       0: terminated normally
%                       1: terminated because it is reached MaxIter
%      itc          - iteration count
%
% Complexity: Ordo(2^N) /plus O(nlogn) for the initial sorting/
% Space     : Ordo(N)
%
% ========================================================================
%                       Comment on the operation
% ========================================================================
% Assumption: items are in decreasing order according to pr(i)/w(i).
% 
% In the first step, we start to fill the initial solution with 1s from
% left to right while we exceed the capacity limit. After this, we select
% that item which exceed the limit and we set it to 0. Then we continue the
% filling with 1s -- until we reach the initial best solution. (So we decide 
% whether a given item is in the knapsack or not.)
%
% After computing the initial best solution, we set the last (from right
% the first one) 1 to 0 (we prune a branch) and we try to find a better 
% solution. (In this situation we use estimators to decide if a 
% brach may contain a better solution or not.) If we find a better solution
% than the initial one we store it as the actual best solution. Then we
% continue this steps.
%
% We find the best solution if we cannot do backtrack. So it provides
% global optimum.
%
% (The binary tree represents tha fact that we select or not select an item
% into a solution: so the tree branch according to this decision.)
%
% ========================================================================
%
% Reference:
% (Martello:knp90)
%   Martello and Toth - Knapsack problems: algorithms and computer implementations, 
%   John Wiley & Sons, Inc., 1990,
%   Chapter 2, 0-1 Knapsack problem
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

chkknapsackinput(prf,wgt,c);

% -- function body --
itc = 0;    % iteration counter

% ordering jobs according to prf/wgt
[b,ix] = sort(prf./wgt,'descend');
p = prf(ix);
w = wgt(ix);

% -- initialization

n = length(p);
z = 0;      % value of best solution so far
x = [];     % best solution so far
zn = 0;     % current objective
cn = c;     % current residual capacity
xn = [];    % current solution
p(n+1) = 0;
w(n+1) = inf;
j = 1;      % search index (!): from this point we are finding a better solution part 
backtrack = false;

% NOTE: A solution is made up of two parts: xn(1:j-1) + xn(j:n). The
% algorithms uses j to add other possible combinations of new solution parts (2nd part) to
% the current solution part (1st part).

while (true)
    
    % it iterates from j=1 to n to calculate a solution (xn(1:n))
    while (j <= n) ...              % not all items are selected/unselected to xn
       && (backtrack == false)      % no need to backtrack (a better solution is possible)

        % ******* Administrative block - Start
        itc = itc + 1;  % increment iteration counter
        if opt.Verbose == 1      % dispay iteration info
            fprintf('%d.',itc);
        end;
        if opt.MaxIter == itc    % reached the maximum iteration
            exitflag = 1;
            x = [];
            z = -1;
            ndx = [];
            return;
        end;
        % ******* Administrative block - End
   
        % computing upperbound from j to n (if j<n)
        if j < n  
            % ------------------------------------------------------------------    
            % -- compute upper bound U1 for the current solution starting from j
            % This BOUND-ing step helps to upperbound the solution which
            % can be (POSSIBLE!) reached at the given branch. If it cannot
            % provide better solution we have to backtrack (alter the branch).
            % NOTE: We can apply any other upperbound estimation method! (e.g. U1-U7)
            % ------------------------------------------------------------------
            % calculate aggregated weights for upperbound calculation
            wagg = [];
            wagg(1) = w(j);                     % aggregated weights
            for k=2:n-j+1
                wagg(k) = wagg(k-1) + w(j+k-1); %#ok<AGROW>
            end;

            % find the first critical item computed from start position j
            r = min(find(wagg > cn));           %#ok<MXFND> 
            
            if isempty(r)       % there is no critical item
                r = length(wagg)+1;
            end;
            
            % U1(j): upperbound of the list from j to (r-1)
            %u = sum(p(j:r-1)) + floor((cn-sum(w(j:r-1)))*p(r)/w(r));
            u = sum(p(j+1:j+r-1)) + floor((cn-sum(w(j+1:j+r-1)))*p(r)/w(r));
        else % upperbound cannot be computed, since it's started from j (j==n)
            u = floor(cn*p(j)/w(j)); %0;
        end;
        
        % checking whether (best solution) < (current solution + U1(j))  
        % so if the sum of zn (x(1...j-1)) + U1(j) is greater then the present best
        % solution -> better solution can be achieved
        if (z < (zn + u))
            % ------------------------------------------------------------------
            % -- perform a forward step: inserting the largest possible set of 
            % new consecutive items into the current solution from position j
            % ------------------------------------------------------------------

            % iterates while (weight(j)) <= (current residual capacity) and
            % incude items into the knapsack (x(j):=1) until they are fitted 
            % /since w(n+1)=inf -> it will stop at the end of weights/
            while w(j) <= cn
                cn = cn - w(j);         % decrease the current remained capacity
                zn = zn + p(j);         % increase the current objective
                xn(j) = 1;              %#ok<AGROW> % select into the knapsack
                j = j + 1;              % increase the start position of the search index
            end;

            % exclude item j (if j<=n) from the solution
            if j <= n
                xn(j) = 0;              %#ok<AGROW> % remove the last item 
                j = j + 1;              % increase the start position of the search index
            end;

            % NOTE: in this point the solution takes the following form sol=[111...10]
            % and j point out to the length(1st part of the solution)+1 position
            
        else % best solution is better then the current possible solution in this branch
            backtrack = true;
        end;

        % in this point it tries (with the while cycle) to fill the remained capacity 
        % (cn) with values 0 or 1 while the solution is fully determined (j = n)

    end;
    
    if backtrack == false % in this point a solution xn(1:n) is computed
        % ------------------------------------------------------------------    
        % -- update the best solution
        % In this branch we reached a better solution then the best
        % solution so far.
        % ------------------------------------------------------------------
        if zn > z   % then let the current solution is the best solution
            z = zn;
            x = xn;
        end;
    end;
    
    % --------------------------------------------------------------------------    
    % -- backtracking: removing the last inserted item from the current solution
    % This step expresses BRANCH-ing facility: change an item selection (from 
    % inclusion to exclusion) therefore alter the branch of the tree where
    % we are searching the best solution.
    % --------------------------------------------------------------------------
    
    % find the last inserted item in the current solution
    i = max(find(xn == 1)); %#ok<MXFND>
    % after leaving the previous outer while loop, the j is equal to n+1
    if i <= min(j,n)
        cn = cn + w(i); % increase residual capacity
        zn = zn - p(i); % decrease profit
        xn(i) = 0;      %#ok<AGROW> % removing last inserted item from the current solution
        j = i + 1;      % set the search index after the removed item
    else
        break;         % there is no possible backtrack therefore we reached the optimum
    end;
    backtrack = false;
end;

% recover the initial order
[b,ix2] = sort(ix);
x = x(ix2);

% determining the indeces
ndx = find(x);

exitflag = 0;   % terminated normally

end
% --- EOF ---