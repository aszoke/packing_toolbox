% # Knapsack Algorithms within Packing Toolbox
%
% ### Features
% 
% * **bkpbb1** - 0-1 knapsack branch and bound algorithm (based on Horowitz-Sahni /1974/)
% * **bkpbruteforce** - 0-1 knapsack brute force algorithm 
% * **bkpci1** - 0-1 knapsack algorithm computing the critical item
% * **bkpdpI1** - BKPDP1 0-1 knapsack dynamic programming algorithm for only integer(!) weights
% * **bkpdpI2** - BKPDP2 0-1 knapsack dynamic programming with memoization (memory function)
% * **bkpgreedy** - 0-1 knapsack greedy algorithm (approximate algorithm: provides local optimum)
% * **bkpreduct1** - 0-1 knapsack algorithm <fname> with reduction
% * **ckp1** - continuous knapsack algorithm (linear programming approach)
% * **ckp2** - continuous knapsack algorithm (linear programming relaxation approach)
%
% * **knapsack_frame** - algorithm frame for different knapsack algorithms
%
% ### Theoretical comments
%
% **LOWER BOUND**: lower bound on the approximation algorithm solution (running time  is bounded by polinomial in the input size)
%
% For a maximization problem, the solution value determined by an approximate algorithm limits the optimal solution value from below. It is always convenient to have methods for limiting this value from above, too: UPPER BOUND.
%
% **UPPER BOUND**:
%
% * in enumerative algorithm: to exclude computations which cannot lead to the optimal solution
% * in approximation algorithm: to "a-posteriori" evaluate the performance obtained Upper bounds are usually computed by solving relaxations such as continuous, Lagrangian and surrogate.
%
% **RELATIVE ERROR** of an approximation algorithm: relative error of the approximate solution is no greater than (U(I)-A(I))/U(I) -- where I is the input; U is the upperbound value, A is the approximation algorithm solution value
%
% **WORST CASE PERFORMANCE RATIO** of an approximation algorithm: the smallest real number such that: U(I)/OPT(I)<=r(U) -- where OPT is the optimal solution value