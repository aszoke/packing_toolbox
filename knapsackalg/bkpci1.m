function [x,z,ndx,exitflag,itc] = bkpci1(prf,wgt,c,opt)
%BKPCI1 0-1 knapsack algorithm computing the critical item
%
% Syntax:
%     [x,z,ndx,exitflag,itc] = bkpci1(prf,wgt,c,opt) 
%   Input params:
%      prf          - profit of elements
%      wgt          - weight of elements
%      c            - capacity of the knapsack
%      opt          - (optional) options for the algorithm execution
%                   - Verbose: 0: (default) diplays no input
%                              1: diplays input for iterations
%                   - MaxIter: []: no barrier
%                              n : maximum number of iteration
%   Return values:
%      x            - unknowns /0,1/ (selected elements into the knapsack)
%      z            - objective (profit)
%      ndx          - index of selected items
%      exitflag     - for identifying the reason the algorithm terminated
%                       0: terminated normally
%                       1: terminated because it is reached MaxIter
%      itc          - iteration count
%
% Complexity: Ordo(N) /plus O(nlogn) for the initial sorting/
% Space     : Ordo(N)
%
%
% Reference:
% (Martello:knp90)
%   Martello and Toth - Knapsack problems: algorithms and computer implementations, 
%   John Wiley & Sons, Inc., 1990,
%   Chapter 2, 0-1 Knapsack problem
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: critical_item

% Copyright 2006-2009

% -- input checking -- 

chkknapsackinput(prf,wgt,c);

% -- function body --

% ordering jobs according to prf/wgt
[b,ix] = sort(prf./wgt,'descend');
p = prf(ix);
w = wgt(ix);

% determining the critical item
[s,exitflag,itc] = critical_item(p,w,c,opt);

% unknowns
x1 = ones(s-1,1)';
x0 = zeros(length(p)-s,1)';
xs = 0;

% determining the solution of KP 
x = [x1 xs x0];
for i=s+1:length(p)
    if w(i) <= (c - sum(w.*x))
        x(i) = 1;
    end;
end;

% recover the initial order
[b,ix2] = sort(ix);
x = x(ix2);
p = p(ix2);

% determining the indeces
ndx = find(x);

% optimal solution value of KP
z = sum(x.*p);

end
% --- EOF ---




