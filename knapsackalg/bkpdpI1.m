function [x,z,ndx,exitflag,itc] = bkpdpI1(prf,wgt,c,opt)
%BKPDP1 0-1 knapsack dynamic programming algorithm for only integer(!) weights
%
% Syntax:
%     [x,z,ndx,exitflag,itc] = bkpdp1(prf,wgt,c,opt) 
%   Input params:
%      prf          - profit of elements
%      wgt          - weight of elements
%      c            - capacity of the knapsack
%      opt          - (optional) options for the algorithm execution
%                   - Verbose: 0: (default) diplays no input
%                              1: diplays input for iterations
%                   - MaxIter: []: no barrier
%                              n : maximum number of iteration
%   Return values:
%      x            - unknowns /0,1/ (selected elements into the knapsack)
%      z            - objective (profit)
%      ndx          - index of selected items
%      exitflag     - for identifying the reason the algorithm terminated
%                       0: terminated normally
%                       1: terminated because it is reached MaxIter
%      itc          - iteration count
%
% ========================================================================
%                       Comment on the operation
% ========================================================================
% Table[i,j] contains the optimal solution of instances (i.e. the value of 
% the most valuable subsets of the first i items that fit into the knapsack 
% capacity of j)
%
% The idea is that: (I) we are building the table which contains the optimal
% combination of items at each capacity (1<=j<=c). So, when computing the
% current instance optimal value we are choosing from (II):
%   a) the last combination (which was the optimal so far), or
%   b) adding the present item to an earlier optimum. This earlier optimum 
%      can be calculated with: 
%       - decreasing the present capacity with the current weight (w(i))
%       - increasing the present profit with the current profit (p(i))
%
% The main characteristics of dynamic programming:
% - Overlapping subproblems -- see (I)
% - Optimal substructure -- see (II)
%
% IMPORTANT: It assumes that weight values are integers. (Table is indexed with weight value.)
%
% ========================================================================
%
% Complexity: Ordo(N*C) /initial sorting is not necessary/
% Space     : -
%
% Reference:
% 1) M. Hristakeva and D. Shrestha: Different Approaches to Solve the 0/1 Knapsack Problem
%    (The presented concept is useful, but there are many problems with the implementation in it.)
% 2)(Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 16, Dynamic programming
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

chkknapsackinput(prf,wgt,c);

if ~isequal(wgt,fix(wgt))
    error('Weights must be integer values!');
end;

% -- function body --
itc = 0;    % iteration counter

n = length(prf);
tbl = zeros(n+1,c+1);   % tabular data type that contains optimal solutions

% for an item index (i) we try to put into the knapsack actual capacity (j): if j < wgt(i)

for i=1:n           % item index
    for j=1:c       % capacity constraint
        
        % ******* Administrative block - Start
        itc = itc + 1;  % increment iteration counter
        if opt.Verbose == 1      % dispay iteration info
            fprintf('%d.',itc);
        end;
        if opt.MaxIter == itc    % reached the maximum iteration
            exitflag = 1;
            x = [];
            z = -1;
            ndx = [];
            return;
        end;
        % ******* Administrative block - End
        
        I = i + 1;  % index for the table to easier understanding
        J = j + 1;  % detto
        if j < wgt(i)                                       % Cannot fit the ith item into the capacity j
            % let the actual optimality equals to the previous one (in row above (I-1) in column J)
            tbl(I,J) = tbl(I-1,J);                  
        else                                                % item ith fits to capacity j
            tbl(I,J) = max(tbl(I-1,J), ...                  % Do not use the ith item (*)
                           tbl(I-1,J-wgt(i)) + prf(i));     % Use the ith item (**)       
        end;
        % (*): at the given capacity constraint (j) the optimum value that
        % is in the previous row (I-1). As a consequence if it is selected,
        % it means that the previous optimum value is true in this step also.
        % (**): if the given ith item fits into the given capality (j), we
        % will check whether it is good to use the previous row optimum (*)
        % or we should use the ith item (**)
        % In other words: adding the profit of the current item (prf(i)) to 
        % the optimum value of the instance of (J-wgt(i)). If it is better 
        % then the previous optimum (I-1,J), then use it.
        % -> 1) into the 1st row we put 0 until wgt(1) > j, otherwise prf(1). 
        %    2) into the 2nd we put the same items as the 1st row until 
        %       wgt(2) > j, otherwise we will check which one has greater 
        %       profit (*) or (**).
    end;
end;

% the following step is to construct the solution (determining which item is 
% included or excluded) from the table

J = c + 1;                      % positioning to the last position in tbl
xn = zeros(1,n);
for i=n:-1:1
    I = i + 1;                  % index for the table for easier understanding
    if tbl(I,J) == tbl(I-1,J)   % it means that ith item is not included in the solution
        xn(i) = 0;
    else
        xn(i) = 1;
        J = J-wgt(i);
    end;
    % 
end;

% right-down corner element is the optimum
z = tbl(n+1,c+1);
% constructed optimal solution
x = xn;
% determining the indeces
ndx = find(x);

exitflag = 0;   % terminated normally

end
% --- EOF ---