# Utils of the Knapsack Algorithms within Packing Toolbox

### Features

* **critical_item** - finds the critical element (the first that is not fit) among the packageable elements
* **chkknapsackinput** - Checking input of knapsack algorithms
* **reduct1** - 0-1 knapsack reduction algorithm (Ingargiola-Kosch /1973/)
