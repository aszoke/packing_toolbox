function chkknapsackinput(prf,wgt,c)
%CHKKNAPSACKINPUT Checking input of knapsack algorithms
%
% Syntax:
%     chkknapsackinput(prf,wgt,c) 
%   Input params:
%      prf          - profit of elements
%      wgt          - weight of elements
%      c            - capacity of the knapsack
%   Return values:
%      -
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% Copyright 2006-2009

% -- function body --

% capacity value checking 
if ~(c > 0)
    error('Capacity must be positive!');
end

% elements checking
if length(wgt) == length(find(wgt > c)) 
    error('Trivially infeasible: At least one element must be less than capacity of knapsack!');
end

% profit checking
if length(prf) ~= length(wgt)
    error('Length of profit vector must be equal to weight vector!');
end;

end
% --- EOF ---