function [s,exitflag,itc] = critical_item(prf,wgt,c,opt)
%CRITICAL_ITEM finds the critical element (the first that is not fit) among the packageable elements
%with continuous relaxation and Dantzig's bound. Balas and Zemel (1980) algorithm.
%
% Syntax:
%   [s,exitflag,itc] = critical_item(prf,wgt,c,opt) 
%   Input params:
%      prf          - profit of elements
%      wgt          - weight of elements
%      c            - capacity of the knapsack
%      opt          - (optional) options for the algorithm execution
%                   - Verbose: 0: (default) diplays no input
%                              1: diplays input for iterations
%                   - MaxIter: []: no barrier
%                              n : maximum number of iteration
%   Return values:
%      s            - index of the critical item
%      exitflag     - for identifying the reason the algorithm terminated
%                       0: terminated normally
%                       1: terminated because it is reached MaxIter
%      itc          - iteration count
%
% Complexity: Ordo(N)
% Space     : Ordo(N)
%
% Comment: it can be easily determine critical item if we can walk through
% the list, but that solution is so slow! 
%
% Reference:
% (Martello:knp90)
%   Martello and Toth - Knapsack problems: algorithms and computer implementations, 
%   John Wiley & Sons, Inc., 1990,
%   Chapter 2, 0-1 Knapsack problem
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

chkknapsackinput(prf,wgt,c);

% -- function body --
itc = 0;    % iteration counter

n = length(prf);
J0 = [];        % set of not selected items (into the knapsack)
J1 = [];        % set of selected items
JC = 1:n;     % set of critical items
cn = c;

partition = false;

% the following method works as the binary searching method: splitting
% intervallums
while ~partition
    
    % ******* Administrative block - Start
    itc = itc + 1;  % increment iteration counter
    if opt.Verbose == 1      % dispay iteration info
        fprintf('%d.',itc);
    end;
    if opt.MaxIter == itc    % reached the maximum iteration
        exitflag = 1;
        return;
    end;
    % ******* Administrative block - End    
    
    R = prf(JC)./wgt(JC);   % computing ratios of JC's items
    lambda = median(R);   % selecting a tentative critical value (heuristic!)
    G = JC(find(R > lambda)); % these items are greater than lambda
    L = JC(find(R < lambda)); % these items are less than lambda
    E = JC(find(R == lambda));% these items are equal to lambda
    c1 = sum(wgt(G));       % summing left side intervall (greater) of critical item
    c2 = c1 + sum(wgt(E));  % left side intervall + equal elements
    if (c1 <= cn) && (cn < c2)  % critical item is between the equal elements
        partition = true;
    elseif c1 > cn          % lambda is too small (less than the critical item)
        J0 = union(J0,union(L,E)); % elements are taken out from the packageable elements
        JC = G;             % new intervall is set to the 'left side'
    else                    % lambda is too large
        J1 = union(J1,union(G,E)); % elements are set to the packageable elements
        JC = L;             % new intervall is set to the 'right side'
        cn = cn - c2;       % since the left side is cutted, therefore the capacity should be decreased 
    end
end
J1 = union(J1,G);   % surely packaged elements
J0 = union(J0,L);   %#ok<NASGU> % surely not packaged elements
JC = E;             % decidable elements (whether they are packed or not)
cn = cn - c1;       % residual capacity

se = zeros(1,length(JC));
se(1) = wgt(JC(1));
for i=2:length(JC)
    se(i) = se(i-1) + wgt(JC(i));
end

% find the first aggragated element that doesn't fit
i = find(se > cn, 1);     % min(find(se > cn))
if isempty(i)
    i = 1;  % if the first element doesn't fit then the next one is the critical
end

s = i + length(J1); % s in the ordered elements

exitflag = 0;   % terminated normally

end
% --- EOF ---

