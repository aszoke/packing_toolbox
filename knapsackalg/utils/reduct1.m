function [J0,J1] = reduct1(prf,wgt,c)
%REDUCT1 0-1 knapsack reduction algorithm (Ingargiola-Kosch /1973/)
%
% Syntax:
%     [J0,J1] = reduct1(prf,wgt,c) 
%   Input params:
%      prf          - profit of elements
%      wgt          - weight of elements
%      c            - capacity of the knapsack
%   Return values:
%      J0           - variables that are surely can be left out
%      J1           - variables that are surely can be put in
%   Note: {J0,J1} are the variables that not necessary compute in a KP
%   algorithm since they are surely neither 0 (J0) or 1 (1). This reducts
%   the original problem to a samller one.
%
% ========================================================================
%                       Comment on the operation
% ========================================================================
% It is about reduction of the problem (n) to a smaller problem (n-|J0|-|J1|) 
% in order to the original problem can be solved with less time. So this is
% just a preprocessing algorithm.
%
% The idea: setting each variable j in turn to 0 if 0<=j<=s or to 1 if
% s<j<=n and calculating the corresponding (actual) continuous upperbound 
% (ZC* -> U1) and the  (actual) optimum (Z*). It is clear that if 
% floor(ZC*)<Z* then the value of the variable xj in the optimal integer 
% solution will be necessary 1 for 0<=j<=s and 0 for s<j<=n.
% ========================================================================
%
% Complexity: Ordo(n^2) /initial sorting is necessary!/
% Space     : -
%
% Reference:
% (Martello:knp90)
%   Martello and Toth - Knapsack problems: algorithms and computer implementations, 
%   John Wiley & Sons, Inc., 1990,
%   Chapter 2, 0-1 Knapsack problem

%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

chkknapsackinput(prf,wgt,c);

% -- function body --

n = length(prf);  % item count

% sw: aggregated (sum) weights
sw = wgt(1);
for i=2:length(wgt)
    sw(i) = sw(i-1) + wgt(i);
end

% Notice that the variables corresponding to items in I1 and J0 must take 
% the fixed value in any optimal solution to KP

J1n = [];                        % surely selected items
J0n = [];                        % surely not selected items

opt = option();                         % create default option object
s = critical_item(prf,wgt,c,opt);       % determining the critical item

l = sum(prf(1:s-1));              % value of an (initial) actual solution

for j=1:s
    % determine critical item depending on which value is removed from the
    % 0<=j<=s interval; -> Note: the index of the selected item (s0) is more than or equal to s
    swj = sw - wgt(j);
    s0 = find(swj > c, 1);

    % computing upperbound while both the previously computed critical item
    % and item j are removed (cannot be the part of the solution) 
    u0 = U1ubound0(s0,j,prf,wgt,c);
    % computing profit but the selected item is excepted from the sum 
    pw0 = sum(prf(1:s0 - 1)) - prf(j);
    % selecting the greater solution
    l = max(l, ...              % actual solution
            pw0);               % new potential solution
    
    % upperbound without j is less than actual optimum value
    if u0 < l 
        J1n = union(J1n,j);
    end
end

for j=s:n
    % determine critical item depending on which value is added to the
    % s<j<=n interval; -> Note: the index of the selected item (s0) is less than  or equal to s
    wj = (c-wgt(j));
    s1 = find(sw > wj, 1);
    
    % computing upperbound using the previously computed critical items
    u1 = U1ubound1(s1,j,prf,wgt,c);
    % computing profit but the critical item is excepted from the sum 
    pw1 = prf(j) + sum(prf(1:s1 - 1));
    % selecting the greater solution
    l = max(l, ...              % actual solution
            pw1);               % new potential solution
    
    % upperbound with j is less than actual optimum value
    if u1 < l 
        J0n = union(J0n,j);
    end   
end

J0 = J0n;
J1 = J1n;

end

% --------------------- SUBFUNCTION ----------------------------

function U1 = U1ubound1(s,j,prf,wgt,c)
% see U1 (Danzig) upper bound - but add item s (critical) and item j
cres = c - sum(wgt(1:s-1)) - wgt(j);
U1 = sum(prf(1:s-1)) + prf(j) + floor((cres/wgt(s))*prf(s));
end

function U1 = U1ubound0(s,j,prf,wgt,c)
% see U1 (Danzig) upper bound - but remove item s (critical) and item j
cres = c - sum(wgt(1:s-1)) + wgt(j);
U1 = sum(prf(1:s-1)) - prf(j) + floor((cres/wgt(s))*prf(s));
end

% --- EOF ---