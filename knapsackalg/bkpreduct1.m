function [x,z,ndx,exitflag,itc] = bkpreduct1(fname,prf,wgt,c,opt)
%BKPREDUCT1 0-1 knapsack algorithm <fname> with reduction
%
% Syntax:
%     [x,z,ndx,exitflag,itc] = bkpreduct1(fname,prf,wgt,c,opt) 
%   Input params:
%      fname        - name of the knapsacking algorithm
%      prf          - profit of elements
%      wgt          - weight of elements
%      c            - capacity of the knapsack
%      opt          - (optional) options for the algorithm execution
%                   - Verbose: 0: (default) diplays no input
%                              1: diplays input for iterations
%                   - MaxIter: []: no barrier
%                              n : maximum number of iteration
%   Return values:
%      x            - unknowns /0,1/ (selected elements into the knapsack)
%      z            - objective (profit)
%      ndx          - index of selected items
%      exitflag     - for identifying the reason the algorithm terminated
%                       0: terminated normally
%                       1: terminated because it is reached MaxIter
%      itc          - iteration count
%
% Complexity: Ordo(N) /initial sorting is not necessary since REDUCT1 includes sorting/
% Space     : Ordo(N)
%
%
% Reference:
% (Martello:knp90)
%   Martello and Toth - Knapsack problems: algorithms and computer implementations, 
%   John Wiley & Sons, Inc., 1990,
%   Chapter 2, 0-1 Knapsack problem
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

chkknapsackinput(prf,wgt,c);

% -- function body --

% ordering jobs according to prf/wgt
[b,ix] = sort(prf./wgt,'descend');
p = prf(ix);
w = wgt(ix);

% determining which variables must be in or not in the optimal solution (reducing the problem)
[J0,J1] = reduct1(p,w,c);

% constructing the reduced problem
pr = p; wr = w;
pr([J0,J1]) = []; wr([J0,J1]) = [];
cr = c - sum(w(J1));

% computing the reduced problem
fh = str2func(fname);           % function handle (determines algorithm)
[xr,z,ndx,exitflag,itc] = fh(pr,wr,cr,opt);

% constructing the solution of the original problem
k = 1;
x = zeros(1,length(p));
for i=1:length(p) 
    if ~isempty(find(J0==i, 1))
        x(i) = 0;
    elseif ~isempty(find(J1==i, 1))
        x(i) = 1;
    else
        x(i) = xr(k);
        k = k + 1;
    end;
end;

% recover the initial order
[b,ix2] = sort(ix);
x = x(ix2);
p = p(ix2);

% determining the indeces
ndx = find(x);

% optimal solution value of KP
z = sum(x.*p);

end

% --- EOF ---