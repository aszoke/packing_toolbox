function display(itm)
%DISPLAY Display items object
%
% Syntax
%    DISPLAY(itm)
%  
% See also ITEMS.

%   Author(s): Akos Szoke
%   Copyright (c) 2006
%   $Revision: 1.0 $  $Date: 2007/01/05 15:41:44 $

disp(' --- Items object --- ');
disp([' Profit:                  ', num2str(itm.Profit)]);
disp([' Weight:                  ', num2str(itm.Weight)]);
disp([' DatasetName:             ', itm.DatasetName]);

% --- EOF ---
