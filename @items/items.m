function items = items(varargin)
%ITEMS Creation of items object.
%
% Class constructors:
%    itm = items() creates a items object
%
%   Author(s): Akos Szoke
%   Copyright (c) 2006
%   $Revision: 1.0 $  $Date: 2007/01/05 15:41:44 $

% -- constructor for no input
if (nargin == 0) 
  
    % Create the structure
    itm = struct(...
        'Module', [],...
        'Profit', [],...
        'Weight', [],...
        'DatasetName', '<N/A>');

    items = class(itm, 'items'); 
    
% -- constructor for other input
else
    error('Unsupported constructor!');
end
    
% --- EOF ---
