function [x,z,ndx,exitflag,out] = glpkwrap(itm,vtype,c)
% GLPKWRAP wrapping function for GLPK (GNU Linear Programming Kit)
%
% Syntax:
%     glpkwrap(itm,vtype,c) 
%   Input params:
%      itm
%		 vtype
%      c
%   Return values:
%      x 
%      y
%      ndx
%      exitflag
%      out
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

varcount = length(itm.Weight);
sense = -1;                                 % Maximization problem
C = itm.Profit;                             % Coefficients of the objective function
a = itm.Weight;                             % Coefficients of the constraints
b = c;                                      % Right hend side value
ctype = 'U';                                % Sense of each constrain (i.e. '<=')
if strcmpi(vtype,'B')
    lb = [];                                % Lowerbound must be empty on BINARY variables
    ub = [];                                % Upperbound must be empty on BINARY variables
else
    lb = zeros(varcount,1);                 % Lowerbound on the decision variables
    ub = ones(varcount,1);                  % Upperbound on the decision variables
end
vartype = [];
for ii=1:varcount                           % Variables for knapsask
    vartype = [vartype; vtype];
end;
param.msglev=1;                             % Output (includes informational messages
[x,z,status,extra] = glpk(C,a,b,lb,ub,ctype,vartype,sense,param);
ndx = find(x);
optim = isequal(status,5);

out.DatasetName         = itm.DatasetName;    
out.IsPacked            = optim;            % TRUE
out.Solution            = x'; 
out.SolutionValue       = z;
out.PackedWeight        = a * x;
out.Bins                = 1;        
out.Iteration           = -1;
if optim
    exitflag = 1;
    if strcmpi(vtype,'B')
        out.PackingAlgorithm    = 'GLPK-bkp';        
    else
        out.PackingAlgorithm    = 'GLPK-ckp';
    end
else
    exitflag = 0;
    if strcmpi(vtype,'B')
        out.PackingAlgorithm    = '*GLPK-bkp';        
    else
        out.PackingAlgorithm    = '*GLPK-ckp';
    end
end
out.Gap                 = b - single(a * x);    % casting because of rounding
out.SolvingTime         = extra.time;
out.Score               = score(out.Gap,out.SolvingTime);
out.Notes               = '';
out.Version             = 1.0;

end