# Utils of the Packing Toolbox

### Features

* **cmpoutset** - display comparison table from output objects
* **cmpsolutionquality** - displays quality comparison table from output objects
* **cmpsolutions** - display comparison table from solutions
* **gap** - computes goodness of the binpacking solution
* **glpkwrap** - wrapping function for GLPK (GNU Linear Programming Kit)
* **score** - score of computation (higher is better)
* **stats** - displays descriptive statistics tabe from input vector
* **strfixlen** - create fixed length string
* **ubounds** - computes several upperbounds of the packing problem
