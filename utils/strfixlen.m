function strfl = strfixlen(str, len)
% STRFIXLEN create fixed length string
%
% Syntax:
%     strfl = strfixlen(str, len) 
%   Input params:
%      str          - input string
%      len          - length of result string
%   Return values:
%      strfl        - resulted string
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

% length checking
if (len < 1)
    error('Length must be greater than zero!');
end

% -- function body --

if (len - length(str)) < 0
    postfix = '...';
    strfl = [str(1:len-3) postfix];
else
    strfl = [str blanks(len - length(str))];
end;

end
% -- EOF --