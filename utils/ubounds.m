function [U,U6,U5,U4,U3,U2,U1] = ubounds(p, w, c)
%UBOUNDS computes several upperbounds of the packing problem
%
% Syntax:
%     [U,U6,U5,U4,U3,U2,U1] = ubounds(p, w, c) 
%   Input params:
%      p            - profit of elements
%      w            - weight of elements
%      c            - capacity of the knapsack
%   Return values:
%      U1           - Danzig (1957) upper bound
%      U2           - Martello-Toth (1977) upper bound
%      U3           - Hudson (1977) & Fayard and Plateau (1982) & Villela and Bomstein (1983) upper bound
%      U4           - M�ller-Merbach (1978) upper bound
%      U5           - Dudzinski-Walukiewicz (1984) upper bound
%      U6           - Martello-Toth (1988) upper bound
%      U            - best upperbound for KP: U = min(U5,U6)
%
% Complexity: see at each upper bound computation
% Space     : -
%
% Comments:
% U2 <= U1
% U3 <= U2 (u0n =< u0; u1 >= u1n)
% U4 < U3
% U5 < U4
% U6 < U4 
% Since there isn't domination between U5 and U6 so the best upperbound for
% KP is U=min(U5,U6)
%
% Reference:
% (Martello:knp90)
%   Martello and Toth - Knapsack problems: algorithms and computer implementations, 
%   John Wiley & Sons, Inc., 1990,
%   Chapter 2.2 RELAXATIONS AND UPPER BOUNDS, 2.3 IMPROVED BOUNDS
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: CRITICAL_ITEM

% Copyright 2006-2008

% -- input checking -- 

% capacity value checking 
if ~(c > 0)
    error('Capacity must be positive!');
end

% elements checking
if length(w) == length(find(w > c)) 
    error('Trivially infeasible: At least one element must be less than capacity of knapsack!');
end

% profit checking
if length(p) ~= length(w)
    error('Length of profit vector must be equal to weight vector!');
end;

% -- function body --

% ordering items according to p/w
[b,ix] = sort(p./w,'descend');
p = p(ix);
w = w(ix);

opt = option();     % default option object
s = critical_item(p,w,c,opt);

% ===== U1 -- Danzig (1957) upper bound =====  
% Martello:knp90 p.17 eq.2.10
% Worst-case performance ratio = 2
% Complexity: Ordo(n)
% IDEA: integral part of the critical item is added to the base part
% proportionally to the residual capacity

% residual capacity -- see p.16 eq.2.9
cres = c - sum(w(1:s-1));

U1 = sum(p(1:s-1)) + floor((cres/w(s))*p(s));

% ===== U2 -- Martello-Toth (1977) upper bound ===== 
% Martello:knp90 p.21 eq.2.16
% Worst-case performance ratio = 2
% Complexity: Ordo(n)
% IDEA: integral part of the best fitted (critical item + 1) or (critical item - 1) 
% is added to the base part proportionally to the residual capacity

% computing u0 (critical item is taken out)
if (s+1) <= length(w)
    u0 = sum(p(1:s-1)) + floor((cres/w(s+1))*p(s+1));
else
    u0 = sum(p(1:s-1));
end;

% computing u1 (critical item is added)
u1 = sum(p(1:s-1)) + floor(p(s) - (w(s) - cres)/w(s-1)*p(s-1));

U2 = max(u0,u1);

% ===== U3 -- Hudson (1977) & Fayard and Plateau (1982) & Villela and Bomstein (1983) upper bound ===== 
% Martello:knp90 p.22 eq.2.19-20
% Worst-case performance ratio = 
% Complexity: Ordo(n)

sw = w(1);
for i=2:length(w)
    sw(i) = sw(i-1) + w(i);
end;

% computing item which should removed while j >= s (critical) item is added
% Note: the index of the selected item (r1(i)) is less than s
for j=s:length(w)
    wj = (c-w(j));              % decreasing the capacity with the added weight of item j
    r1(j) = find(sw > wj, 1);   % (minimal) critical item which exceeds the decreased capacity
end;
r1s = r1(s);                 

% an equivalent computation (!!!):
r1s = find(sw > (c-w(s)), 1); 

% computing item which should be added while j< s (critical) item is removed
% Note: the index of the selected item (r0(j)) is more than s
for j=1:s
    swj = sw - w(j);
    r0(j) = find(swj > c, 1);   % (minimal) critical item which exceeds the decreased capacity
end;
r0s = r0(s);

% an equivalent computation (!!!):
r0s = find(sw - w(s) > c, 1);

% computing u1n
sw1 = sum(w(1:r1s - 1));
pw1 = sum(p(1:r1s - 1));
u1n = p(s) + pw1 + floor((c - w(s) - sw1) / w(r1s) * p(r1s));

% computing u0n
sw0 = sum(w(1:r0s - 1)) - w(s);
pw0 = sum(p(1:r0s - 1)) - p(s);
u0n = pw0 + floor((c - sw0) / w(r0s) * p(r0s));

U3 = max(u0n,u1n);

% ===== U4 -- M�ller-Merbach (1978) upper bound ===== 
% Martello:knp90 p.23 eq.2.21
% Worst-case performance ratio = 

zckp = sum(p(1:s-1)) + (cres/w(s))*p(s);
for j=1:length(w)
    pstar(j) = p(j)-w(j)/w(s)*p(s);
end;
pstar2 = pstar;
pstar2(s) = [];
m = max(zckp - abs(pstar2));
U4 = max(sum(p(1:s-1)),m);

% ===== U5 -- Dudzinski-Walukiewicz (1984) upper bound ===== 
% Martello:knp90 p.24 eq.2.22
% Worst-case performance ratio = 

xk = horzcat(ones(1,s-1),zeros(1,length(w)-(s-1)));
for i=s:length(w)
    if w(i) <= (c - sum(w .* xk))
        xk(i) = 1;
    end;
end;

m1 = max(zckp - pstar(1:s-1));
Ns = find(xk == 0);
N = Ns(find(Ns ~= s));
m2 = max(zckp + pstar(N));
sxk = sum(p .* xk);
mm = horzcat(min(u1n,m1),min(u0n,m2),sxk);
U5 = max(mm);

% ===== U6 -- Martello-Toth (1988) upper bound ===== 
% Martello:knp90 p.25 eq.2.24
% Worst-case performance ratio = 
% Complexity: Ordo(n)for computing critical item plus Ordo(2^(t-r)) to
% explore the binary tree

% TODO: programming the U6 computation
U6 = [];

% ===== U -- best upper bound ===== 
% Martello:knp90 p.26 
% Worst-case performance ratio = 

U = min(U5,U6);
