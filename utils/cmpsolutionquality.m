function cmpsolutionquality(outset,c,anul)
% CMPSOLUTIONQUALITY displays quality comparison table from output objects
%
% Syntax:
%     cmpsolutionquality(outset) 
%   Input params:
%      outset          - output objects in cell
%      c               - capacity
%      anul            - whether the last bin is anulled
%   Return values:
%      - 
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2010

% -- input checking -- 

% length checking
if (length(outset) < 1)
    error('Length must be greater than zero!');
end

% -- function body --

DSLEN = 20;
PALEN = 25;
SVLEN = 25;
DLLEN = 15;

heading = sprintf('| %s | %s | %s | %s | %s | %s | %s |'...
    , strfixlen('DatasetName',DSLEN)...
    , strfixlen('PackingAlgorithm',PALEN)...
    , strfixlen('Value',SVLEN)...      
    , strfixlen('D >= 20%',DLLEN)...
    , strfixlen('20% > D >= 10%',DLLEN)...
    , strfixlen('10% > D > 0%',DLLEN)...
    , strfixlen('D = 0%',DLLEN));
titleline = [];
for i=1:length(heading)
    titleline = [titleline '-'];
end;
    
prefix = '| *** ';
postfix = ' *** |';
title = 'Comparison of Algorithms (QUALITY)';
sumspace = length(heading) - length(title) - length(prefix) - length(postfix);
spacebefore = floor(sumspace / 2);
spaceafter  = sumspace - spacebefore;

disp(titleline);
disp([prefix blanks(spacebefore) title blanks(spaceafter) postfix]);
disp(titleline);
disp(heading);
disp(titleline);
dsndx = 1;
out = output();                                  
out.DatasetName = 'Fictive';        % for summary
outset = [outset,{out}];
for i=1:length(outset)-1
    
    v = outset{i}.SolutionValue;
    if anul
        v2 = v(1:length(v)-1);      % the last bin is anulled
    else
        v2 = v(1:length(v));
    end;
    delta = c(1) - v2;
    % FIXME: it assumes that every iteration lenght is equal to the first
    % one!
    d10 = c(1)*0.1;
    d20 = c(1)*0.2;
    D1 = index2count(find(delta >= d20));
    D2 = index2count(find(delta >= d10)) - D1;
    D3 = index2count(find(delta > 0)) - D1 - D2;
    D4 = index2count(find(v2, c(1))) - D1 - D2 - D3;    
    
    fprintf('| %s | %s | %s | %s | %s | %s | %s | \n'...
        , strfixlen(outset{i}.DatasetName,DSLEN)...
        , strfixlen(outset{i}.PackingAlgorithm,PALEN)...
        , strfixlen(mat2str(outset{i}.SolutionValue),SVLEN)...        
        , strfixlen(mat2str(D1),DLLEN)...
        , strfixlen(mat2str(D2),DLLEN)...
        , strfixlen(mat2str(D3),DLLEN)...
        , strfixlen(mat2str(D4),DLLEN));

    if ~strcmp(outset{i}.DatasetName,outset{i+1}.DatasetName)   % new dataset occured
        disp(titleline);
        dsndx = i+1;                                              % increase start
    end 

end;

end

function v = index2count(i)
    if isempty(i)                                               % if there is no any items
        v = 0;
    else
        v = length(i);                                           % count the number of items
    end;
end


% -- EOF --