function s = score(gapsize, ctime)
% SCORE score of computation (higher is better)
%
% Syntax:
%     s = score(gapsize, ctime)
%   Input params:
%      gapsize      - blank (not filled) space in the knapsack
%      ctime        - time of computation
%   Return values:
%      s            - score
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

% gap size value checking 
if (gapsize < 0)
    error('Gap size cannot be negative!');
end

% computation time value checking 
if (ctime < 0)
    error('Computation time cannot be negative!');
end

% -- function body --

% Alpha is used to increase the relative importance of the gap measure.  This ensures
% that a very fast, very stupid entry with zero time but with awful performance does not win.
alpha = 0.018;
% For differentiating global optimum solutions (they equally yield zero gap)
beta = 0.001;
% For easier reading
gamma = 1000;

s = gamma * (gapsize + beta) / (ctime + alpha);

end
% -- EOF --