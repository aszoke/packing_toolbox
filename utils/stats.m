function stats(dat,datname)
% STATS displays descriptive statistics tabe from input vector
%
% Syntax:
%     stats(data) 
%   Input params:
%      dat          - input vector of data
%      datname      - name of input data
%   Return values:
%      - 
%
% ========================================================================
%                       Comment on the statistics
% ========================================================================
% - Skewness: is a measure of the asymmetry of the data around the sample
% mean. If skewness is negative, the data are spread out more to the left of 
% the mean than to the right. If skewness is positive, the data are spread 
% out more to the right. The skewness of the normal distribution (or any 
% perfectly symmetric distribution) is zero.
% - Kurtosis: is a measure of how outlier-prone a distribution is. The
% kurtosis of the normal distribution is 3. Distributions that are more 
% outlier-prone than the normal distribution have kurtosis greater than 3; 
% distributions that are less outlier-prone have kurtosis less than 3.
% - Coefficient of variation (CV): (a sz�r�s h�ny %-a az �tlagnak /vari�ci�s t�nyez�/)
% It is a normalized measure of dispersion of a probability distribution. 
% It is defined as the ratio of the standard deviation (sigma) to the mean.
% ========================================================================
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

tStart=tic;
% -- input checking -- 

% -- function body --

DSLEN = 15;
MNLEN = 10;
MDLEN = 10;
MILEN = 10;
MALEN = 10;
SDLEN = 10;
CVLEN = 10;
SKLEN = 10;
KULEN = 10;

heading = sprintf('| %s | %s | %s | %s | %s | %s | %s | %s | %s |'...
    , strfixlen('DatasetName',DSLEN)...    
    , strfixlen('Mean',MNLEN)...
    , strfixlen('Median',MDLEN)...
    , strfixlen('Min',MILEN)...
    , strfixlen('Max',MALEN)...
    , strfixlen('St dev',SDLEN)...
    , strfixlen('Coeff var',CVLEN)...
    , strfixlen('Skewness',SKLEN)...
    , strfixlen('Kurtosis',KULEN));
titleline = [];
for i=1:length(heading)
    titleline = [titleline '-'];
end;
    
prefix = '| *** ';
postfix = ' *** |';
title = 'Statistics';
sumspace = length(heading) - length(title) - length(prefix) - length(postfix);
spacebefore = floor(sumspace / 2);
spaceafter  = sumspace - spacebefore;

disp(titleline);
disp([prefix blanks(spacebefore) title blanks(spaceafter) postfix]);
disp(titleline);
disp(heading);
disp(titleline);

for i=1:length(dat)
    fprintf('| %s | %s | %s | %s | %s | %s | %s | %s | %s | \n'...
        , strfixlen(datname{i},DSLEN)...
        , strfixlen(num2str(mean(dat{i})),MNLEN)...
        , strfixlen(num2str(median(dat{i})),MDLEN)...
        , strfixlen(num2str(min(dat{i})),MILEN)...
        , strfixlen(num2str(max(dat{i})),MALEN)...
        , strfixlen(num2str(std(dat{i})),SDLEN)...
        , strfixlen(num2str(std(dat{i})/mean(dat{i})),CVLEN)...    
        , strfixlen(num2str(skewness(dat{i})),SKLEN)...
        , strfixlen(num2str(kurtosis(dat{i})),KULEN));
    disp(titleline);
end

disp(['STATS is taken ' num2str(toc(tStart)) ' sec.']);

end
% -- EOF --