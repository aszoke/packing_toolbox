function cmpoutset(outset)
% CMPOUTSET displays comparison table from output objects
%
% Syntax:
%     cmpoutset(outset) 
%   Input params:
%      outset          - output objects in cell
%   Return values:
%      - 
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

% length checking
if (length(outset) < 1)
    error('Length must be greater than zero!');
end

% -- function body --

DSLEN = 20;
PALEN = 25;
ITLEN = 10;
GPLEN = 10;
STLEN = 16;
SCLEN = 10;

heading = sprintf('| %s | %s | %s | %s | %s | %s |'...
    , strfixlen('DatasetName',DSLEN)...
    , strfixlen('PackingAlgorithm',PALEN)...
    , strfixlen('Iteration',ITLEN)...
    , strfixlen('Gap',GPLEN)...
    , strfixlen('SolvingTime (ms)',STLEN)...
    , strfixlen('Score',SCLEN));
titleline = [];
for i=1:length(heading)
    titleline = [titleline '-'];
end;
    
prefix = '| *** ';
postfix = ' *** |';
title = 'Comparison of Algorithms (PERFORMANCE)';
sumspace = length(heading) - length(title) - length(prefix) - length(postfix);
spacebefore = floor(sumspace / 2);
spaceafter  = sumspace - spacebefore;

disp(titleline);
disp([prefix blanks(spacebefore) title blanks(spaceafter) postfix]);
disp(titleline);
disp(heading);
disp(titleline);
dsndx = 1;
out = output();                                  
out.DatasetName = 'Fictive';        % for summary
outset = [outset,{out}];
for i=1:length(outset)-1
    fprintf('| %s | %s | %s | %s | %s | %s | \n'...
        , strfixlen(outset{i}.DatasetName,DSLEN)...
        , strfixlen(outset{i}.PackingAlgorithm,PALEN)...
        , strfixlen(num2str(outset{i}.Iteration),ITLEN)...
        , strfixlen(num2str(outset{i}.Gap),GPLEN)...
        , strfixlen(num2str(outset{i}.SolvingTime * 1000),STLEN)...
        , strfixlen(num2str(outset{i}.Score),SCLEN));

    if ~strcmp(outset{i}.DatasetName,outset{i+1}.DatasetName)   % new dataset occured
        st = [];
        for ii=dsndx:i
            st = [st outset{ii}.SolvingTime];
        end
        [c,qi] = min(st);                               % quickest index
        disp(titleline);
        disp(['| --> Quickest:', outset{dsndx + qi -1}.PackingAlgorithm,]);
        disp(titleline);
        dsndx = i;                                      % increase start
    end

end;

end
% -- EOF --