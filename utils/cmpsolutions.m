function cmpsolutions(outset)
% CMPSOLUTIONS displays comparison table from solutions
%
% Syntax:
%     cmpsolutions(outset) 
%   Input params:
%      outset          - solutions (output.Solution). The first solution is
%      the reference solution.
%   Return values:
%      - 
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

% length checking
if (length(outset) < 2)
    error('Length must be greater than one!');
end

% -- function body --

DSLEN = 25;
PALEN = 20;
SLLEN = 40;
SVLEN = 25;
WGLEN = 25;
OKLEN = 5;

heading = sprintf('| %s | %s | %s | %s | %s | %s |'...
    , strfixlen('DatasetName',DSLEN)...
    , strfixlen('PackingAlgorithm',PALEN)...
    , strfixlen('Solution',SLLEN)...
    , strfixlen('Value',SVLEN)...    
    , strfixlen('Weight',WGLEN)...    
    , strfixlen('OK?',OKLEN));
titleline = [];
for i=1:length(heading)
    titleline = [titleline '-'];
end;
    
prefix = '| *** ';
postfix = ' *** |';
title = 'Comparison of Algorithms (SOLUTION)';
sumspace = length(heading) - length(title) - length(prefix) - length(postfix);
spacebefore = floor(sumspace / 2);
spaceafter  = sumspace - spacebefore;

disp(titleline);
disp([prefix blanks(spacebefore) title blanks(spaceafter) postfix]);
disp(titleline);
disp(heading);
disp(titleline);

dsndx = 1;
out = output();                                  
out.DatasetName = 'Fictive';        % for summary
outset = [outset,{out}];
for i=1:length(outset)-1
    
    if dsndx == i
        strok = 'REF';
    else
        strok = num2str(isequal(outset{dsndx}.Solution,outset{i}.Solution));
    end;
      
    fprintf('| %s | %s | %s | %s | %s | %s | \n'...
        , strfixlen(outset{i}.DatasetName,DSLEN)...
        , strfixlen(outset{i}.PackingAlgorithm,PALEN)...
        , strfixlen(mat2str(outset{i}.Solution),SLLEN)...
        , strfixlen(mat2str(outset{i}.SolutionValue),SVLEN)...        
        , strfixlen(mat2str(outset{i}.PackedWeight),WGLEN)...        
        , strfixlen(strok,OKLEN));

    if ~strcmp(outset{i}.DatasetName,outset{i+1}.DatasetName)   % new dataset occured
        disp(titleline);
        dsndx = i+1;                                              % increase start
    end    
end

end
% -- EOF --