function res = gap(bins,c)
% GAP computes goodness of the binpacking solution
%
% Syntax:
%     res = gap(bins,c) 
%   Input params:
%      bins         - used up capacities in bins
%      c            - capacities of the bins
%   Return values:
%      res          - blank (not filled) space in the bins
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking -- 

% capacity value checking 
if ~(c > 0)
    error('Capacity must be positive!');
end

% -- function body --

scap = single(sum(c(1:length(bins))));      % Casting to single for small differences
sbin = single(sum(bins(1:length(bins))));   % Casting to single for small differences
res  = (scap - sbin) / scap;

end

% -- EOF --