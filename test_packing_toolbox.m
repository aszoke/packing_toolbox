%% PACKING TOOLBOX - SAMPLE TEST SET
% Packing Toolbox contains knapsacking and binpacking algorithms. Knapsacking and binpacking problems are a class of optimization problems that involve attempting to pack objects together into containers. 
%
% *Author*: Akos Szoke (aszoke@mit.bme.hu)
%
% *Web site*: <http://www.kese.hu>
%
% *Toolbox Source*: <https://bitbucket.org/aszoke/>
%
%%
dbstop if error;
clear;

load data_binpack;

disp(' ***************************************************** ');
disp(' ******** BINPACKING ALGORITHMS TESTS: Start ********* ')
disp(' ***************************************************** ');

outset = [];

for i=1:length(dataset)
    itm = items();
    itm.Profit      = zeros(1,length(dataset{i}.w));
    itm.Weight      = dataset{i}.w;
    itm.DatasetName = dataset{i}.name;
    c               = capacity{i};
    opt = option();
    %opt.Verbose = 1;
    opt.MaxIter = 100;
    
    disp([' ****** ', itm.DatasetName, ' ****** ']);
        
    disp(' *** BBPGREEDY greedy bin-packing algorithms with FF strategy (local optimum) *** ');
    opt.Strategy = 'FF';
    [X,z,NDX,exitflag,out] = binpack_frame('bbpgreedy',itm,c,opt);
    [X,z,NDX,exitflag] = bbpgreedy(itm.Weight,c,opt);
    outset = [outset,{out}];

    disp(' *** BBPGREEDY greedy bin-packing algorithms with BF strategy (local optimum) *** ');
    opt.Strategy = 'BF';
    [X,z,NDX,exitflag,out] = binpack_frame('bbpgreedy',itm,c,opt);
    [X,z,NDX,exitflag] = bbpgreedy(itm.Weight,c,opt);
    outset = [outset,{out}];
    
    disp(' *** BBPGREEDY greedy bin-packing algorithms with FFD strategy (local optimum) *** ');
    opt.Strategy = 'FFD';
    [X,z,NDX,exitflag,out] = binpack_frame('bbpgreedy',itm,c,opt);
    [X,z,NDX,exitflag] = bbpgreedy(itm.Weight,c,opt);
    outset = [outset,{out}];
        
    disp(' *** BBPGREEDY greedy bin-packing algorithms with BFD strategy (local optimum) *** ');
    opt.Strategy = 'BFD';
    [X,z,NDX,exitflag,out] = binpack_frame('bbpgreedy',itm,c,opt);
    [X,z,NDX,exitflag] = bbpgreedy(itm.Weight,c,opt);
    outset = [outset,{out}];
    
end

cmpoutset(outset);
cmpsolutions(outset);

disp(' *********************************************** ');
disp(' ******** BINPACKING UTILITY TEST: End ********* ')
disp(' *********************************************** ');

%%
dbstop if error;
clear;

load data_knapsack;

disp(' ***************************************************** ');
disp(' ******* KNAPSACKING ALGORITHMS TEST: Start ********** ')
disp(' ***************************************************** ');

outset = [];

for i=1:length(dataset)
    itm = items();
    itm.Profit      = dataset{i}.p;
    itm.Weight      = dataset{i}.w;
    itm.DatasetName = dataset{i}.name;
    c               = capacity{i};
    opt = option();
    %opt.Verbose = 1;
    opt.MaxIter = 100000;
    
    disp([' ****** ', itm.DatasetName, ' ****** ']);
    
    disp(' *** Critical item computation *** ');
    s = critical_item(itm.Profit,itm.Weight,c,opt)

    disp(' *** Several upperbound computing algorithms *** ');
    [U,U6,U5,U4,U3,U2,U1] = ubounds(itm.Profit,itm.Weight,c);
    
%     disp(' *** GLPK-bkp: ');
%     [x,z,ndx,exitflag,out] = glpkwrap(itm,'B',c);
%     outset = [outset,{out}];     

%     disp(' *** GLPK-ckp: ');
%     [x,z,ndx,exitflag,out] = glpkwrap(itm,'C',c);
%     outset = [outset,{out}];  
    
    % -------------------------------------------------------------    
    
    disp(' *** 0-1 binpacking critical_item-based algorithm (local optimum) *** ');
    [x,z,ndx,exitflag,out] = knapsack_frame('bkpci1',itm,c,opt);
    [x,z,ndx,exitflag] = bkpci1(itm.Profit,itm.Weight,c,opt);
    outset = [outset,{out}];   
    
    disp(' *** Continuous knapsacking using LP relaxation algorithm *** ');
    [x,z,ndx,exitflag,out] = knapsack_frame('ckp1',itm,c,opt);
    [x,z,ndx,exitflag] = ckp1(itm.Profit,itm.Weight,c,opt);
    outset = [outset,{out}];   

    disp(' *** Binary knapsacking algorithm utilizing CKP algorithm *** ');
    % 0-1 knapsacking using Linear Programming relaxation (based on CKP)
    % Continuous knapsacking algorithm
    [x,z,ndx,exitflag,out] = knapsack_frame('ckp2',itm,c,opt);
    [x,z,ndx,exitflag] = ckp2(itm.Profit,itm.Weight,c,opt); % continuous binpacking using Linear Programming relaxation approach
    outset = [outset,{out}];   
    
    disp(' *** Brute force binary knapsacking algorithm (global optimum) *** ');
    [x,z,ndx,exitflag,out] = knapsack_frame('bkpbruteforce',itm,c,opt);
    [x,z,ndx,exitflag] = bkpbruteforce(itm.Profit,itm.Weight,c,opt);
    outset = [outset,{out}];   
    
    disp(' *** Greedy binary knapsacking algorithm (local optimum) *** ');
    [x,z,ndx,exitflag,out] = knapsack_frame('bkpgreedy',itm,c,opt);
    [x,z,ndx,exitflag] = bkpgreedy(itm.Profit,itm.Weight,c,opt);
    outset = [outset,{out}];   

    disp(' *** Dynamic programming binary knapsacking algorithm for only integer(!) weights (global optimum) *** ');
    [x,z,ndx,exitflag,out] = knapsack_frame('bkpdpI1',itm,c,opt);
    [x,z,ndx,exitflag] = bkpdpI1(itm.Profit,itm.Weight,c,opt);
    outset = [outset,{out}];
    
    disp(' *** Dynamic programming with memoization (memory function) binary knapsacking algorithm for only integer(!) weights (global optimum) *** ');
    [x,z,ndx,exitflag,out] = knapsack_frame('bkpdpI2',itm,c,opt);
    [x,z,ndx,exitflag] = bkpdpI2(itm.Profit,itm.Weight,c,opt);
    outset = [outset,{out}];
    
    disp(' *** Branch-and-bound based on Horowitz-Sahni binary knapsacking algorithm (global optimum) *** ');
    [x,z,ndx,exitflag,out] = knapsack_frame('bkpbb1',itm,c,opt);
    [x,z,ndx,exitflag] = bkpbb1(itm.Profit,itm.Weight,c,opt);
    outset = [outset,{out}];
    
    disp(' *** bkpbb1 With reduction Ingargiola-Kosch binary knapsacking algorithm (global optimum) *** ');
    [x,z,ndx,exitflag,out] = knapsack_frame('bkpreduct1',itm,c,opt);
    [x,z,ndx,exitflag] = bkpreduct1('bkpbb1',itm.Profit,itm.Weight,c,opt);
    outset = [outset,{out}];
end;

cmpoutset(outset);
cmpsolutions(outset);

disp(' *********************************************** ');
disp(' ******* KNAPSACKING ALGORITHMS TEST: End ****** ')
disp(' *********************************************** ');

