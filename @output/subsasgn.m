function a = subsasgn(a,index,val)
% SUBSASGN for items objects
%
% Syntax:
%  a = SUBSASGN(a,index,val)
%
% Usage:
%   items.<fieldname> = <fieldvalue> 
%
%  See also ITEMS.

%   Author(s): Akos Szoke
%   Copyright (c) 2006
%   $Revision: 1.0 $  $Date: 2007/01/05 15:41:44 $

switch index(1).type
case '.'
    try
        a.(index(1).subs) = val; % using dynamic fields
    catch ME
        disp('Invalid field reference!');
        rethrow(ME);        
    end
otherwise
   error('Subscripted reference error occured!')
end

% --- EOF ---
