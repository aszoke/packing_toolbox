function display(out)
%DISPLAY Display output object
%
% Syntax
%    DISPLAY(out)
%  
% See also OUTPUT.

%   Author(s): Akos Szoke
%   Copyright (c) 2006
%   $Revision: 1.0 $  $Date: 2007/01/05 15:41:44 $
disp(' --- Output object --- ');
disp([' DatasetName:             ', out.DatasetName]);
disp([' IsPacked:                ', num2str(out.IsPacked)]);
disp([' PackingAlgorithm:        ', out.PackingAlgorithm]);
disp([' Solution:                ', mat2str(out.Solution)]);
disp([' SolutionValue:           ', num2str(out.SolutionValue)]);
disp([' PackedWeight:            ', num2str(out.PackedWeight)]);
disp([' Bins:                    ', num2str(out.Bins)]);
disp([' Iteration:               ', num2str(out.Iteration)]);
disp([' Gap:                     ', num2str(out.Gap)]);
disp([' SolvingTime:             ', num2str(out.SolvingTime)]);
disp([' Score:                   ', num2str(out.Score)]);
disp([' Notes:                   ', out.Notes]);
disp([' Version:                 ', num2str(out.Version)]);

% --- EOF ---
