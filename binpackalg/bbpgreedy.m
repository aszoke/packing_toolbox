function [X,z,Ndx,exitflag,itc] = bbpgreedy(wgt,c,opt)
%BBPGREEDY greedy bin-packing algorithms with packing strategies (approximate algorithm: provides local optimum)
%
% Syntax:
%     [X,z,Ndx,exitflag,itc] = BBPGREEDY(wgt,c,opt) 
%   Input params:
%      wgt          - elements that must be put into a bin (weights - wgt(j))
%      c            - capacity of each bin
%      opt          - (optional) options for the algorithm execution
%                   - Verbose: 0: (default) diplays no input
%                              1: diplays input for iterations
%                   - MaxIter: []: no barrier
%                              n : maximum number of iteration
%                   - Strategy:'FF', 'BF', 'FFD', 'BFD' (see below)
%   Return values:
%      X            - assignment matrix. X(i,j)= 1 if wgt(i) is assigned to bin(j)
%      z            - objective (profit == WEIGHT!)
%      Ndx          - index of selected items
%      exitflag     - for identifying the reason the algorithm terminated
%                       0: terminated normally
%                       1: terminated because it is reached MaxIter
%      itc          - iteration count
%
% Complexity: Ordo(N*logN) (sorting is included)
% Space     : Ordo(N)
%
% Comments on heuristics strategies (see reference!):
%   FF: First-Fit (FF), considers the items according to increasing
%       indices and assigns each item to the lowest indexed initialized bin
%       into which it fits
%   BF: is obtained from FF by assigning the current item to the feasible bin 
%       (if any) having the smallest residual capacity (breaking ties in favour 
%       of the lowest indexed bin)
%   FFD: sort object in decreasing order and then use first fit (FF)
%   BFD: sort object in decreasing order and then use best fit (BF)
%
% Reference:
% (Martello:knp90)
%   Martello and Toth - Knapsack problems: algorithms and computer implementations, 
%   John Wiley & Sons, Inc., 1990,
%   Chapter 8, Bin-packing problems
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking --

chkbinpackinput(wgt,c);

% -- function body --
itc = 0;    % iteration counter

% determine strategy for packing
switch upper(opt.Strategy)
    case {'FF','BF'} % FF, BF
    case {'FFD','BFD'} % FFD, BFD
        [wgt,idec] = sort(wgt,'descend');
        [tmp,irec]=sort(idec); % irec is the recovery ordering
    otherwise 
        error('Unknown strategy!');
end

% load data
n = length(wgt);          % count of elements
r = c*ones(1,n);          % residual capacities of each bin
X = zeros(n);             % assignment matrix: column-bin, row-element 

for j=1:n
    
    % ******* Administrative block - Start
    itc = itc + 1;  % increment iteration counter
    if opt.Verbose == 1      % dispay iteration info
        fprintf('%d.',itc);
    end;
    if opt.MaxIter == itc    % reached the maximum iteration
        exitflag = 1;
        X = [];
        Ndx = [];
        return;
    end;
    % ******* Administrative block - End    
    
    switch upper(opt.Strategy)
        case {'FF','FFD'} % FF, FFD 
           % -- applying FIRST FIT strategy
           % find: returns bins where wgt(j) can be fitted
           idx0 = find(wgt(j) <= r); %#ok<MXFND>
           % min: returns a bin which has the lowest index (FIRST FIT)
           idx=min(idx0);
        case {'BF','BFD'} % BF, BFD 
           % -- applying BEST FIT strategy
           % returns indeces that has greater or equal residual than zero
           idx0=find(r-wgt(j) >= 0);
           % returns a bin to where it is best fitted and has the lowest
           % index (BEST FIT) 
           [val,idx1]=min(r(idx0));
           % return the index of possible bin that is best fitted
           idx = idx0(idx1);
    end;
   % -- update assignment matrix
   % 1) set '1' in the matrix where column denotes bins, and row denotes
   % element
   X(j,idx)=1; 
   % 2) capacity of the bin is decreased by wgt(j) /value of the element/
   r(idx)=r(idx)-wgt(j);  % update the state
end

% in case of *Decreasing (*D) assignment must be reordered with 'irec'
if (strcmpi(opt.Strategy,'FFD') || strcmpi(opt.Strategy,'BFD')) 
    X = X(irec,:); % sort it back to the original assignment order
end

% number of bins used
binsused = size(find(sum(X)>0),2);
% determining the indeces
Ndx = cell(1,binsused);
z = zeros(1,binsused);
for i=1:binsused
    Ndx(i) = {find(X(:,i))};
    z(i) = sum(wgt(Ndx{i}));
end;

exitflag = 0;   % terminated normally

end

% --- EOF ---
