function [X,z,Ndx,exitflag,out] = binpack_frame(fname,itm,cap,opt)
%BINPACK_FRAME algorithm frame for different binpack algorithms
%
% Syntax:
%     [X,ndx,exitflag,output] = binpack_frame(items,cap,opt)
%   Input params:
%      fname        - binpack algorithm's name
%      items        - packagable elements (with profit and weights)
%      cap          - capacity of the bins
%      opt          - (optional) options for the algorithm execution
%                   - verbose: 0: (default) diplays no input
%                              1: diplays input for iterations
%   Return values:
%      X            - unknown matrix /0,1/ (selected elements into the bins)
%      z            - objective (profit == WEIGHT!)
%      Ndx          - index of selected items
%      exitflag     - for identifying the reason the algorithm terminated
%                       0: terminated normally
%                       1: terminated
%      out          - output object
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- function body --

fh = str2func(fname);           % function handle (determines algorithm)

tic;                % --> START computation stopwatch

[X,z,Ndx,exitflag,itc] = fh(itm.Weight,cap,opt);     %TODO: bbpgreedy doesn't deal with profit

ctime = toc;        % --> STOP computation stopwatch

% --- set output properties ---
out = output();

out.Iteration           = itc;
out.DatasetName         = itm.DatasetName;

if exitflag == 0        % % terminated normally
    out.PackingAlgorithm    = [fname,opt.Strategy];
    out.IsPacked            = 1;    % TRUE
    out.Solution            = X;
    out.SolutionValue       = z;
    out.PackedWeight        = itm.Weight * X;
    out.Bins                = length(Ndx);
    out.SolvingTime         = ctime;
    out.Notes               = '';
    out.Version             = 1.0;
else
    out.PackingAlgorithm    = ['*', fname,opt.Strategy];
end

end
% --- EOF ---