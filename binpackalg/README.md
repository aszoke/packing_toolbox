# Binpacking Algorithms within Packing Toolbox

### Features

* **bbpgreedy** - greedy bin-packing algorithms with packing strategies (approximate algorithm: provides local optimum)

* **binpack_frame** - algorithm frame for different binpack algorithms
