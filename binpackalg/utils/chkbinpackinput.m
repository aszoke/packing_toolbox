function chkbinpackinput(wgt,c)
%CHKBINPACKINPUT Checking input of binpack algorithms
%
% Syntax:
%     chkbinpackinput(wgt,c) 
%   Input params:
%      wgt          - weight of elements
%      c            - capacity of the knapsack
%   Return values:
%      -
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% Copyright 2006-2009

% -- function body --

% capacity value checking 
if ~(c > 0)
    error('Capacity must be positive!');
end

% elements checking
if ~isempty(find(wgt > c)) %#ok<EFIND>
    error('All elements must be less than capacity of bins!');
end

end

% --- EOF ---